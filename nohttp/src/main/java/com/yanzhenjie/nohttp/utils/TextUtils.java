package com.yanzhenjie.nohttp.utils;
/*----------------------------------------------------------------------------
 * Copyright (c) <2013-2018>, <Huawei Technologies Co., Ltd>
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 * to endorse or promote products derived from this software without specific prior written
 * permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Notice of Export Control Law
 * ===============================================
 * Huawei LiteOS may be subject to applicable export control laws and regulations, which might
 * include those applicable to Huawei LiteOS of U.S. and the country in which you are located.
 * Import, export and usage of Huawei LiteOS in any manner by you shall be in compliance with such
 * applicable export control laws and regulations.
 *---------------------------------------------------------------------------*/
import ohos.agp.text.Font;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TextUtils {
    /**
     * font file name: digit condensed regular
     */
    public static final String DIGIT_REGULAR = "hw-digit-reg-LL.otf";

    /**
     * font file name: digit condensed medium
     */
    public static final String DIGIT_MEDIUM = "hw-digit-med-LL.otf";

    /**
     * font file name: robot condensed regular
     */
    public static final String ROBOT_CONDENSED_REGULAR = "sans-serif-condensed";

    /**
     * font file name: robot condensed medium
     */
    public static final String ROBOT_CONDENSED_MEDIUM = "sans-serif-condensed-medium";

    /**
     * up to one line
     */
    public static final int MAX_LINE_ONE = 1;

    /**
     * up to two lines
     */
    public static final int MAX_LINE_TWO = 2;

    /**
     * text content max line
     */
    public static final int PRIVACY_TEXT_MAX_LINE = 50;

    /**
     * text content max line
     */
    public static final int STOP_TEXT_MAX_LINE = 10;

    /**
     * font name: default medium
     */
    public static final String MEDIUM = "medium";

    /**
     * font name: default regular
     */
    public static final String REGULAR = "regular";

    private static final int FONT_WEIGHT_MEDIUM = 500;

    private static final String TAG = "TextUtils";

    private static final int DECIMAL_DIGITS = 2;

    private static final float CELSIUS_TO_FAHRENHEIT_CONSTANT = 32f;

    private static final float CELSIUS_TO_FAHRENHEIT_RATIO = 1.8f;

    private static final int BUFFER_LENGTH = 8192;

    private static final long TIME_SHOW_ONE_WIDE_CHAR = 500L;

    private static final String RAW_FILE_PATH = "entry_watch/resources/rawfile/";

    private static final Map<String, Font> FONT_MAP = new HashMap<>();

    private static NumberFormat sNumberFormat;

    private TextUtils() {
    }

    /**
     * check if the input is empty
     *
     * @param input the input string
     * @return the input string is empty
     */
    public static boolean isEmpty(String input) {
        return input == null || input.length() == 0;
    }

    /**
     * check if the input string is empty
     *
     * @param input the input strings
     * @return the input string is empty
     */
    public static boolean isEmpty(String... input) {
        if (input == null) {
            return true;
        }
        for (String oneItem : input) {
            if (isEmpty(oneItem)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a string containing the tokens joined by delimiters.
     *
     * @param tokens an array objects to be joined. Strings will be formed from
     *     the objects by calling object.toString().
     * @param delimiter Returns
     * @return Returns
     */
    public static String join(CharSequence delimiter, Iterable tokens) {
        StringBuilder sb = new StringBuilder();
        Iterator<?> it = tokens.iterator();
        if (it.hasNext()) {
            sb.append(it.next());
            while (it.hasNext()) {
                sb.append(delimiter);
                sb.append(it.next());
            }
        }
        return sb.toString();
    }

}

