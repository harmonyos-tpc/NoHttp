/*
 * Copyright © Yan Zhenjie. All Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yanzhenjie.nohttp;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * <p>Poster.</p>
 * Created on 2016/6/7.
 *
 * @author Yan Zhenjie.
 */
public final class HandlerDelivery {

    private static HandlerDelivery instance;

    public static HandlerDelivery getInstance() {
        if (instance == null)
            synchronized (HandlerDelivery.class) {
                if (instance == null)
                    instance = new HandlerDelivery(new EventHandler(EventRunner.getMainEventRunner()));
            }
        return instance;
    }

    private EventHandler mHandler;

    private HandlerDelivery(EventHandler handler) {
        this.mHandler = handler;
    }

    public void post(Runnable r) {
         mHandler.postTask(r);
    }

    public void postDelayed(Runnable r, long delayMillis) {
         mHandler.postTimingTask(r, delayMillis);
    }

    public void postAtFrontOfQueue(Runnable r) {
         mHandler.postTask(r);
    }

    public void postAtTime(Runnable r, long uptimeMillis) {
         mHandler.postTimingTask(r, uptimeMillis);
    }

    public void postAtTime(Runnable r, Object token, long uptimeMillis) {
         mHandler.postTimingTask(r,uptimeMillis);
    }

    public void removeCallbacks(Runnable r) {
        mHandler.removeTask(r);
    }

    public void removeCallbacks(Runnable r, Object token) {
        mHandler.removeTask(r);
    }
}
