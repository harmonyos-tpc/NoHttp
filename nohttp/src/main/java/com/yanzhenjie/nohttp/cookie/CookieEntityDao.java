/*
 * Copyright 2015 Yan Zhenjie
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yanzhenjie.nohttp.cookie;

import com.yanzhenjie.nohttp.db.BaseDao;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Cookie database manager.</p>
 * Created in Dec 18, 2015 7:01:31 PM.
 *
 * @author Yan Zhenjie.
 */
public class CookieEntityDao extends BaseDao<CookieEntity> {

    public CookieEntityDao(Context context) {
        super(false,context);
    }

    /**
     * Add or update by index(name, domain, path).
     *
     * @param cookie cookie entity.
     * @return columnName
     */
    @Override
    public long replace(CookieEntity cookie) {
        RdbStore database = getWriter();
        database.beginTransaction();

        ValuesBucket values = new ValuesBucket();
        values.putString(CookieSQLHelper.URI, cookie.getUri());
        values.putString(CookieSQLHelper.NAME, cookie.getName());
        values.putString(CookieSQLHelper.VALUE, cookie.getValue());
        values.putString(CookieSQLHelper.COMMENT, cookie.getComment());
        values.putString(CookieSQLHelper.COMMENT_URL, cookie.getCommentURL());
        values.putString(CookieSQLHelper.DISCARD, String.valueOf(cookie.isDiscard()));
        values.putString(CookieSQLHelper.DOMAIN, cookie.getDomain());
        values.putLong(CookieSQLHelper.EXPIRY, cookie.getExpiry());
        values.putString(CookieSQLHelper.PATH, cookie.getPath());
        values.putString(CookieSQLHelper.PORT_LIST, cookie.getPortList());
        values.putString(CookieSQLHelper.SECURE, String.valueOf(cookie.isSecure()));
        values.putInteger(CookieSQLHelper.VERSION, cookie.getVersion());
        try {
            long result = database.replace(CookieSQLHelper.TABLE_NAME, values);
            database.markAsCommit();
            return result;
        } catch (Exception e) {
            return -1;
        } finally {
            database.endTransaction();
            closeDateBase(database);
        }
    }

    @Override
    protected List<CookieEntity> getList(String querySql) {
        RdbStore database = getReader();
        List<CookieEntity> cookies = new ArrayList<>();
        ResultSet cursor = database.querySql(querySql, null);
        while (!cursor.isClosed() && cursor.goToNextRow()) {
            CookieEntity cookie = new CookieEntity();
            cookie.setId(cursor.getInt(cursor.getColumnIndexForName(CookieSQLHelper.ID)));
            cookie.setUri(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.URI)));
            cookie.setName(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.NAME)));
            cookie.setValue(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.VALUE)));
            cookie.setComment(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.COMMENT)));
            cookie.setCommentURL(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.COMMENT_URL)));
            cookie.setDiscard("true".equals(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.DISCARD))));
            cookie.setDomain(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.DOMAIN)));
            cookie.setExpiry(cursor.getLong(cursor.getColumnIndexForName(CookieSQLHelper.EXPIRY)));
            cookie.setPath(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.PATH)));
            cookie.setPortList(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.PORT_LIST)));
            cookie.setSecure("true".equals(cursor.getString(cursor.getColumnIndexForName(CookieSQLHelper.SECURE))));
            cookie.setVersion(cursor.getInt(cursor.getColumnIndexForName(CookieSQLHelper.VERSION)));
            cookies.add(cookie);
        }
        closeCursor(cursor);
        closeDateBase(database);
        return cookies;
    }

    @Override
    protected String getTableName() {
        return CookieSQLHelper.TABLE_NAME;
    }
}