/*
 * Copyright 2015 Yan Zhenjie
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yanzhenjie.nohttp;

import com.yanzhenjie.nohttp.tools.IOUtils;
import com.yanzhenjie.nohttp.utils.LogUtil;
import ohos.media.image.ImagePacker;
import ohos.media.image.PixelMap;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <p>
 * A default implementation of Binary.
 * All the methods are called in Son thread.
 * </p>
 * Created in Oct 17, 2015 12:40:54 PM.
 *
 * @deprecated use {@link FileBinary} instead.
 */
@Deprecated
public class BitmapBinary extends BasicBinary {

    private PixelMap mBitmap;

    /**
     * An input stream {@link Binary}.
     *
     * @param bitmap   image.
     * @param fileName file name. Had better pass this value, unless the server tube don't care about the file name.
     */
    public BitmapBinary(PixelMap bitmap, String fileName) {
        this(bitmap, fileName, null);
    }

    /**
     * An input stream {@link Binary}.
     *
     * @param bitmap   image.
     * @param fileName file name. Had better pass this value, unless the server tube don't care about the file name.
     * @param mimeType such as: image/png.
     */
    public BitmapBinary(PixelMap bitmap, String fileName, String mimeType) {
        super(fileName, mimeType);
        if (bitmap == null)
            throw new IllegalArgumentException("Bitmap is null: " + fileName);
        if (bitmap.isReleased())
            throw new IllegalArgumentException("Bitmap is recycled: " + fileName + ", bitmap must be not recycled.");
        this.mBitmap = bitmap;
    }

    @Override
    protected InputStream getInputStream() throws IOException {
        if (mBitmap.isReleased()) return null;
        return new ByteArrayInputStream(bitmap2ByteArray(mBitmap));
    }

    @Override
    public long getBinaryLength() {
        if (mBitmap.isReleased()) return 0;
        return bitmap2ByteArray(mBitmap).length;
    }

    public static byte[] bitmap2ByteArray(PixelMap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        ImagePacker imagePacker = ImagePacker.create();
        ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
        packingOptions.format = "image/jpeg";
        packingOptions.quality = 100;
        boolean result = imagePacker.initializePacking(outputStream, packingOptions);
        if(result){
            imagePacker.addImage(bitmap);
            long dataSize = imagePacker.finalizePacking();
            LogUtil.info("bitmap编码--->",String.valueOf(dataSize));
        }

        IOUtils.closeQuietly(outputStream);
        return outputStream.toByteArray();
    }
}