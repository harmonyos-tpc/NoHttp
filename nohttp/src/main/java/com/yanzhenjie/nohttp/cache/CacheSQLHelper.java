/*
 * Copyright 2015 Yan Zhenjie
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yanzhenjie.nohttp.cache;

import com.yanzhenjie.nohttp.db.BasicSQLHelper;
import ohos.data.rdb.RdbStore;


/**
 * <p>CacheStore database operation class.</p>
 * Created in Jan 10, 2016 12:39:15 AM.
 *
 * @author Yan Zhenjie;
 */
public class CacheSQLHelper extends BasicSQLHelper {

    static final String DB_CACHE_NAME = "_nohttp_cache_db.db";
    static final int DB_CACHE_VERSION = 3;
    static final String TABLE_NAME = "cache_table";
    static final String KEY = "key";
    static final String HEAD = "head";
    static final String DATA = "data";
    static final String LOCAL_EXPIRES = "local_expires";

    private static final String SQL_CREATE_TABLE = "CREATE TABLE cache_table" +
            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT, head TEXT, data text, local_expires text)";
    private static final String SQL_CREATE_UNIQUE_INDEX = "CREATE UNIQUE INDEX cache_unique_index ON cache_table(\"key\")";
    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS cache_table";

    @Override
    public void onCreate(RdbStore rdbStore) {
        rdbStore.beginTransaction();
        try {
            rdbStore.executeSql(SQL_CREATE_TABLE);
            rdbStore.executeSql(SQL_CREATE_UNIQUE_INDEX);
            rdbStore.markAsCommit();
        } finally {
            rdbStore.endTransaction();
        }
    }

    @Override
    public void onUpgrade(RdbStore rdbStore, int i, int i1) {
        if (i1 != i) {
            rdbStore.beginTransaction();
            try {
                rdbStore.executeSql(SQL_DELETE_TABLE);
                rdbStore.executeSql(SQL_CREATE_TABLE);
                rdbStore.executeSql(SQL_CREATE_UNIQUE_INDEX);
                rdbStore.markAsCommit();
            } finally {
                rdbStore.endTransaction();
            }
        }
    }
}
