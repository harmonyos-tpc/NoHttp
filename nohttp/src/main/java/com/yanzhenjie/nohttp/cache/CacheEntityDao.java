/*
 * Copyright 2015 Yan Zhenjie
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yanzhenjie.nohttp.cache;

import com.yanzhenjie.nohttp.Logger;
import com.yanzhenjie.nohttp.db.BaseDao;
import com.yanzhenjie.nohttp.tools.Encryption;

import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;


/**
 * <p>CacheStore database manager.</p>
 * Created in Jan 10, 2016 12:42:29 AM.
 *
 * @author Yan Zhenjie;
 */
public class CacheEntityDao extends BaseDao<CacheEntity> {

    private Encryption mEncryption;

    public CacheEntityDao(Context context) {
        super(true,context);
        String encryptionKey = context.getApplicationInfo().getProcess();
        mEncryption = new Encryption(encryptionKey);


    }

    @Override
    public long replace(CacheEntity cacheEntity) {
        RdbStore database = getWriter();
        database.beginTransaction();
        try {
            ValuesBucket values = new ValuesBucket();
            values.putString(CacheSQLHelper.KEY, cacheEntity.getKey());
            values.putString(CacheSQLHelper.HEAD, encrypt(cacheEntity.getResponseHeadersJson()));
            values.putString(CacheSQLHelper.DATA, encrypt(Base64.getEncoder().encodeToString(cacheEntity.getData())));
            values.putString(CacheSQLHelper.LOCAL_EXPIRES, encrypt(Long.toString(cacheEntity.getLocalExpire())));
            long result = database.replace(getTableName(), values);
            database.markAsCommit();
            return result;
        } catch (Exception e) {
            return -1;
        } finally {
            database.endTransaction();
            closeDateBase(database);
        }
    }

    @Override
    protected List<CacheEntity> getList(String querySql) {
        RdbStore database = getReader();
        List<CacheEntity> cacheEntities = new ArrayList<>();
        ResultSet resultSet = null;
        try {
             resultSet = database.querySql(querySql, null);
            while (!resultSet.isClosed() && resultSet.goToNextRow()) {
                CacheEntity cacheEntity = new CacheEntity();
                cacheEntity.setId(resultSet.getInt(resultSet.getColumnIndexForName(CacheSQLHelper.ID)));
                cacheEntity.setKey(resultSet.getString(resultSet.getColumnIndexForName(CacheSQLHelper.KEY)));
                cacheEntity.setResponseHeadersJson(decrypt(resultSet.getString(resultSet.getColumnIndexForName(CacheSQLHelper.HEAD))));
                cacheEntity.setData(Base64.getDecoder().decode(decrypt(resultSet.getString(resultSet.getColumnIndexForName(CacheSQLHelper.DATA)))));
                cacheEntity.setLocalExpire(Long.parseLong(decrypt(resultSet.getString(resultSet.getColumnIndexForName(CacheSQLHelper.LOCAL_EXPIRES)))));
                cacheEntities.add(cacheEntity);
            }
        } catch (Exception e) {
            Logger.e(e);
        } finally {
            closeCursor(resultSet);
            closeDateBase(database);
        }
        return cacheEntities;
    }

    @Override
    protected String getTableName() {
        return CacheSQLHelper.TABLE_NAME;
    }

    private String encrypt(String encryptionText) throws Exception {
        return mEncryption.encrypt(encryptionText);
    }

    private String decrypt(String cipherText) throws Exception {
        return mEncryption.decrypt(cipherText);
    }
}
