/*
 * Copyright 2015 Yan Zhenjie
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yanzhenjie.nohttp.rest;
import com.yanzhenjie.nohttp.Headers;
import com.yanzhenjie.nohttp.Logger;
import com.yanzhenjie.nohttp.RequestMethod;
import ohos.agp.components.Image;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;


import java.util.Locale;

/**
 * <p>Image handle parameter.</p>
 * Created in Oct 17, 2015 12:17:57 PM.
 *
 * @author Yan Zhenjie.
 */
public class ImageRequest extends Request<PixelMap> {

    private final int mMaxWidth;
    private final int mMaxHeight;
    private final PixelFormat mDecodeConfig;
    private Image.ScaleMode mScaleType;


    /**
     * Decoding lock so that we don't decode more than one image at a time (to avoid OOM's).
     */
    private static final Object DECODE_LOCK = new Object();

    public ImageRequest(String url, RequestMethod requestMethod, int maxWidth, int maxHeight,
                        PixelFormat decodeConfig, Image.ScaleMode scaleMode) {
        super(url, requestMethod);
        this.mMaxWidth = maxWidth;
        this.mMaxHeight = maxHeight;
        this.mDecodeConfig = decodeConfig;
        this.mScaleType = scaleMode;
        setAccept("image/*");
    }

    @Override
    public PixelMap parseResponse(Headers responseHeaders, byte[] responseBody) throws Exception {
        synchronized (DECODE_LOCK) {
            PixelMap bitmap = null;
            if (responseBody != null) {
                try {
                    bitmap = doResponse(responseBody);
                } catch (OutOfMemoryError e) {
                    String errorMessage = String.format(Locale.US, "Caught OOM for %d byte image, url=%s",
                            responseBody.length, url());
                    Logger.e(e, errorMessage);
                }
            }
            return bitmap;
        }
    }

    /**
     * The real guts of AnalyzeResponse. Broken out for readability.
     *
     * @param byteArray Remarks
     * @throws null
     * @return Remarks
     */
    private PixelMap doResponse(byte[] byteArray) throws OutOfMemoryError {
        ImageSource.DecodingOptions decodeOptions = new ImageSource.DecodingOptions();
        PixelMap bitmap;
        if (mMaxWidth == 0 && mMaxHeight == 0) {
            decodeOptions.desiredPixelFormat = mDecodeConfig;

            ImageSource imageSource = ImageSource.create(byteArray, 0, byteArray.length, null);
            bitmap = imageSource.createPixelmap(decodeOptions);
        } else {
            //TODO API映射结果无法替换
//            decodeOptions.inJustDecodeBounds = true;

            ImageSource imageSource = ImageSource.create(byteArray, 0, byteArray.length, null);
            bitmap = imageSource.createPixelmap(decodeOptions);

            int actualWidth = imageSource.getImageInfo(0).size.width;
            int actualHeight = imageSource.getImageInfo(0).size.height;
//            int actualWidth = decodeOptions.outWidth;
//            int actualHeight = decodeOptions.outHeight;

            int desiredWidth = getResizedDimension(mMaxWidth, mMaxHeight, actualWidth, actualHeight, mScaleType);
            int desiredHeight = getResizedDimension(mMaxHeight, mMaxWidth, actualHeight, actualWidth, mScaleType);
            //TODO API映射结果无法替换
//            decodeOptions.inJustDecodeBounds = false;

//            decodeOptions.sampleSize = findBestSampleSize(actualWidth, actualHeight, desiredWidth, desiredHeight);
//            Bitmap tempBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, decodeOptions);
            ImageSource tempImageSource = ImageSource.create(byteArray, 0, byteArray.length, null);
            PixelMap tempBitmap = tempImageSource.createPixelmap(decodeOptions);

            if (tempBitmap != null && (tempBitmap.getImageInfo().size.width > desiredWidth || tempBitmap.getImageInfo().size.height >
                    desiredHeight)) {

//                bitmap = Bitmap.createScaledBitmap(tempBitmap, desiredWidth, desiredHeight, true);
                Rect rect = new Rect();
                rect.width=desiredWidth;
                rect.height=desiredHeight;

                PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
                initializationOptions.size = new Size(desiredWidth, desiredHeight);
                initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
                bitmap=  PixelMap.create(tempBitmap, rect,initializationOptions);
                tempBitmap.release();
            } else {
                bitmap = tempBitmap;
            }
        }
        return bitmap;
    }

    private static int getResizedDimension(int maxPrimary, int maxSecondary, int actualPrimary, int actualSecondary,
                                           Image.ScaleMode scaleMode) {

        // If no dominant value at all, just return the actual.
        if ((maxPrimary == 0) && (maxSecondary == 0)) {
            return actualPrimary;
        }

        // If ScaleType.FIT_XY fill the whole rectangle, ignore ratio.
        if (scaleMode == Image.ScaleMode.ZOOM_CENTER) {
            if (maxPrimary == 0) {
                return actualPrimary;
            }
            return maxPrimary;
        }

        // If primary is unspecified, scale primary to match secondary's scaling ratio.
        if (maxPrimary == 0) {
            double ratio = (double) maxSecondary / (double) actualSecondary;
            return (int) (actualPrimary * ratio);
        }

        if (maxSecondary == 0) {
            return maxPrimary;
        }

        double ratio = (double) actualSecondary / (double) actualPrimary;
        int resized = maxPrimary;

        // If ScaleType.CENTER_CROP fill the whole rectangle, preserve aspect ratio.
        if (scaleMode == Image.ScaleMode.STRETCH) {
            if ((resized * ratio) < maxSecondary) {
                resized = (int) (maxSecondary / ratio);
            }
            return resized;
        }

        if ((resized * ratio) > maxSecondary) {
            resized = (int) (maxSecondary / ratio);
        }
        return resized;
    }

    // Visible for testing.
    public static int findBestSampleSize(int actualWidth, int actualHeight, int desiredWidth, int desiredHeight) {
        double wr = (double) actualWidth / desiredWidth;
        double hr = (double) actualHeight / desiredHeight;
        double ratio = Math.min(wr, hr);
        float n = 1.0f;
        while ((n * 2) <= ratio) {
            n *= 2;
        }
        return (int) n;
    }

}