package com.yanzhenjie.nohttp.db;


import com.yanzhenjie.nohttp.cache.CacheSQLHelper;
import com.yanzhenjie.nohttp.cookie.CookieSQLHelper;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;


public class DBCreat {
    static final String DB_CACHE_NAME = "_nohttp_cache_db.db";
    static final int DB_CACHE_VERSION = 3;


    private static final String DB_COOKIE_NAME = "_nohttp_cookies_db.db";
    private static final int DB_COOKIE_VERSION = 2;

    /**
     * columnName
     *
     * @param bl      ture cache    ;     false cookie
     * @param context
     * @return columnName
     */
    public static RdbStore getRdbStore(boolean bl, Context context) {

        RdbOpenCallback rdbOpenCallback = null;

        StoreConfig config = StoreConfig.newDefaultConfig(bl ? DB_CACHE_NAME : DB_COOKIE_NAME);
        if (bl) {
            rdbOpenCallback = new CacheSQLHelper();
        } else {
            rdbOpenCallback = new CookieSQLHelper();
        }
        DatabaseHelper helper = new DatabaseHelper(context);
        RdbStore store = helper.getRdbStore(config, bl ? DB_CACHE_VERSION : DB_COOKIE_VERSION, rdbOpenCallback, null);
        return store;
    }
}


