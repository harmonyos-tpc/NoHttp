/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.http;


import com.yanzhenjie.nohttp.Headers;
import com.yanzhenjie.nohttp.Logger;
import com.yanzhenjie.nohttp.RequestMethod;
import com.yanzhenjie.nohttp.rest.Request;
import com.yanzhenjie.nohttp.tools.HeaderUtils;
import com.yanzhenjie.nohttp.tools.IOUtils;


/**
 * <p>基本请求，添加Appkey、Nonce、Timestamp、Signature。</p> Created by Yan Zhenjie on 2016/11/16.
 */
public abstract class AbstractRequest<Entity>
        extends Request<Result<Entity>> {
    private static final String TAG = "AbstractRequest------";

    public AbstractRequest(String url, RequestMethod requestMethod) {
        super(url, requestMethod);
        setAccept(Headers.HEAD_VALUE_CONTENT_TYPE_JSON);
    }

    @Override
    public void onPreExecute() {
    }

    @Override
    public final Result<Entity> parseResponse(Headers responseHeaders, byte[] responseBody) throws Exception {
        String body = "";
        if (responseBody != null && responseBody.length > 0) {
            body = parseResponseString(responseHeaders, responseBody);
        }
        Logger.i("服务器数据：" + body);

        int resCode = responseHeaders.getResponseCode();

        // Http层成功，这里只可能业务逻辑错误。
        if (resCode >= 200 && resCode <= 300) {
            HttpEntity httpEntity;

            if (resCode == 200) {
                httpEntity = new HttpEntity();
                httpEntity.setmSucceed(true);
                httpEntity.setmData(body);
                httpEntity.setmMessage("请求成功！！！");
            } else {
                httpEntity = new HttpEntity();
                httpEntity.setmSucceed(false);
                httpEntity.setmMessage("服务器数据格式错误");
            }

            if (httpEntity.ismSucceed()) { // 业务成功，解析data。
                try {
                    Entity result = parseEntity(httpEntity.getmData());
                    return new Result<>(true, responseHeaders, result, resCode, null);
                } catch (Throwable throwable) {
                    // 解析data失败，data和预期不一致，服务器返回格式错误。
                    return new Result<>(false, responseHeaders, null, resCode,
                            "服务器数据格式错误");
                }
            } else {
                // The server failed to read the wrong information.
                return new Result<>(false, responseHeaders, null, resCode, httpEntity.getmMessage());
            }

        } else if (resCode >= 400 && resCode < 500) {
            return new Result<>(false, responseHeaders, null, resCode,
                    "发声未知异常");
        }
        return new Result<>(false, responseHeaders, null, resCode,
                "服务器开小差啦");
    }

    /**
     * 把数据解析为泛型对象。
     *
     * @param responseBody 响应包体。
     * @return 泛型对象。
     * @throws com.alibaba.fastjson.JSONException String
     */
    protected abstract Entity parseEntity(String responseBody) throws Throwable;

    private static String parseResponseString(Headers responseHeaders, byte[] responseBody) {
        if (responseBody == null || responseBody.length == 0) {
            return "";
        }
        String charset = HeaderUtils.parseHeadValue(responseHeaders.getContentType(), "charset", "");
        return IOUtils.toString(responseBody, charset);
    }
}