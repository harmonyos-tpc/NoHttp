/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.http;


import com.yanzhenjie.nohttp.NoHttp;
import com.yanzhenjie.nohttp.download.DownloadListener;
import com.yanzhenjie.nohttp.download.DownloadQueue;
import com.yanzhenjie.nohttp.download.DownloadRequest;

/**
 * Created by Yan Zhenjie on 2016/12/20.
 */
public class Download {

    private static Download instance;

    public static Download getInstance() {
        if (instance == null) {
            synchronized (Download.class) {
                if (instance == null) {
                    instance = new Download();
                }
            }
        }
        return instance;
    }

    private DownloadQueue mDownloadQueue;

    /**
     * Download
     */
    private Download() {
        mDownloadQueue = NoHttp.newDownloadQueue();
    }

    /**
     * download
     *
     * @param what download
     * @param mRequest download
     * @param mListener download
     */
    public void download(int what, DownloadRequest mRequest, DownloadListener mListener) {
        mDownloadQueue.add(what, mRequest, mListener);
    }

    /**
     * cancelBySign
     *
     * @param sign cancelBySign
     */
    public void cancelBySign(Object sign) {
        mDownloadQueue.cancelBySign(sign);
    }

    /**
     * cancelAll
     */
    public void cancelAll() {
        mDownloadQueue.cancelAll();
    }

}
