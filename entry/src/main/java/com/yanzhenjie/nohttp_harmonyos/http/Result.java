/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.http;


import com.yanzhenjie.nohttp.Headers;

/**
 * <p>封装的请求结果。</p>
 * Created by Yan Zhenjie on 2016/11/23.
 */
public class Result<T> {

    /**
     * 业务是否成功。
     */
    private boolean isSucceed;
    /**
     * 是否来自缓存的数据。
     */
    private boolean isFromCache;
    /**
     * 响应头。
     */
    private Headers headers;
    /**
     * 结果。
     */
    private T tt;
    /**
     * 业务代码，目前和Http相同。
     */
    private int mLogicCode;

    /**
     * 错误消息。
     */
    private String message;

    Result(boolean isSucceed, Headers headers, T tg, int logicCode, String message) {
        this.isSucceed = isSucceed;
        this.headers = headers;
        this.tt = tg;
        this.mLogicCode = logicCode;
        this.message = message;
    }

    /**
     * 业务是否成功。
     *
     * @return String
     */
    public boolean isSucceed() {
        return isSucceed;
    }

    /**
     * 是否是框架级别的错误。
     *
     * @return String
     */
    public boolean isLocalError() {
        return mLogicCode == -1;
    }

    /**
     * 设置是否是来自缓存的数据。
     *
     * @param fromCache String
     */
    void setFromCache(boolean fromCache) {
        isFromCache = fromCache;
    }

    /**
     * 是否是来自缓存的数据。
     *
     * @return String
     */
    public boolean isFromCache() {
        return isFromCache;
    }

    /**
     * Http响应头。
     *
     * @return String
     */
    public Headers headers() {
        return headers;
    }

    /**
     * 请求data体结果。
     *
     * @return String
     */
    public T get() {
        return tt;
    }

    /**
     * 业务状态码。
     *
     * @return String
     */
    public int getLogicCode() {
        return mLogicCode;
    }

    /**
     * 服务器错误信息。
     *
     * @return String
     */
    public String error() {
        return message;
    }
}
