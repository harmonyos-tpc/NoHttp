/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.http;

import com.alibaba.fastjson.JSON;
import com.yanzhenjie.nohttp.RequestMethod;
import com.yanzhenjie.nohttp.utils.TextUtils;



/**
 * <p>请求实体。</p>
 * Created by Yan Zhenjie on 2016/11/16.
 */


public class EntityRequest<Entity> extends AbstractRequest<Entity> {

    private Class<Entity> clazz;

    public EntityRequest(String url, RequestMethod requestMethod, Class<Entity> clazz) {
        super(url, requestMethod);
        this.clazz = clazz;
    }

    @Override
    protected Entity parseEntity(String responseBody) throws Throwable {
        if (TextUtils.isEmpty(responseBody)) {
            return null;
        } else {
            return JSON.parseObject(responseBody, clazz);
        }
    }
}
