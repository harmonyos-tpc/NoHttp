/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.http;

import com.yanzhenjie.nohttp.error.NetworkError;
import com.yanzhenjie.nohttp.error.TimeoutError;
import com.yanzhenjie.nohttp.error.URLError;
import com.yanzhenjie.nohttp.error.UnKnownHostError;
import com.yanzhenjie.nohttp.rest.OnResponseListener;
import com.yanzhenjie.nohttp.rest.Response;
import ohos.app.Context;


/**
 * <p>Harmonized Http results callback.</p> Created by Yan Zhenjie on 2016/7/7.
 */
public class DefaultResponseListener<T> implements OnResponseListener<Result<T>> {
    private Context mContext;
    private HttpCallback<T> mHttpCallback;


    public DefaultResponseListener(Context context, HttpCallback<T> callback) {
        this.mContext = context;
        this.mHttpCallback = callback;


    }

    @Override
    public void onStart(int what) {

    }

    @Override
    public void onFinish(int what) {

    }

    @Override
    public void onSucceed(int what, Response<Result<T>> response) {
        Result<T> tResult = response.get();
        tResult.setFromCache(response.isFromCache());
        if (mHttpCallback != null) {
            mHttpCallback.onResponse(tResult);
        }
    }

    @Override
    public void onFailed(int what, Response<Result<T>> response) {
        Exception exception = response.getException();
        String stringRes = "发声未知异常";
        if (exception instanceof NetworkError) {
            stringRes = "网络不可用，请检查网络";
        } else if (exception instanceof TimeoutError) {
            stringRes = "连接网络超时";
        } else if (exception instanceof UnKnownHostError) {
            stringRes = "没有找到url制定服务器";
        } else if (exception instanceof URLError) {
            stringRes = "url格式错误啦";
        }
        if (mHttpCallback != null) {
            Result<T> errorResult = new Result<>(false, response.getHeaders(), null, -1, stringRes);
            mHttpCallback.onResponse(errorResult);
        }
    }
}