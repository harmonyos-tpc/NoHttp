/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.http;


import com.yanzhenjie.nohttp.rest.*;
import com.yanzhenjie.nohttp.utils.LogUtil;

/**
 * Created by YanZhenjie on 2018/7/25.
 */
public class LoginInterceptor implements Interceptor {

    @Override
    public <T> Response<T> intercept(RequestHandler requestHandler, Request<T> request) {
        Response<T> tResponse = requestHandler.handle(request);
        if (tResponse.isSucceed()) {
            LogUtil.info("ok","ok");
        }
        return tResponse;
    }
}