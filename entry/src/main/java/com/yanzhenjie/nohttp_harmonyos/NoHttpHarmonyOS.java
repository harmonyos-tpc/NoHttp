/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos;
import ohos.aafwk.ability.AbilityPackage;

/**
 * AbilityPackage
 */
public class NoHttpHarmonyOS extends AbilityPackage {
    private static NoHttpHarmonyOS instance;

    @Override
    public void onInitialize() {
        super.onInitialize();

        if (instance == null) {
            instance = this;
        }
    }

    /**
     * dsds
     *
     * @return NoHttpHarmonyOS
     */
    public static NoHttpHarmonyOS getApp() {
        return instance;
    }


}
