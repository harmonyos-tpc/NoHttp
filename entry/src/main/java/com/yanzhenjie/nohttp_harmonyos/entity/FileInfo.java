/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.entity;


import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by YanZhenjie on 2018/3/29.
 */
public class FileInfo {

    @JSONField(name = "name")
    private String mName;
    @JSONField(name = "password")
    private String mPassword;
    @JSONField(name = "file1")
    private String mFile1;
    @JSONField(name = "file2")
    private String mFile2;
    @JSONField(name = "file3")
    private String mFile3;

    /**
     * getName
     *
     * @return getName
     */
    public String getName() {
        return mName;
    }

    /**
     * setName
     *
     * @param name setName
     */
    public void setName(String name) {
        mName = name;
    }

    /**
     * getPassword
     *
     * @return getPassword
     */
    public String getPassword() {
        return mPassword;
    }

    /**
     * getPassword
     *
     * @param password getPassword
     */
    public void setPassword(String password) {
        mPassword = password;
    }

    /**
     * getFile1
     *
     * @return getFile1
     */
    public String getFile1() {
        return mFile1;
    }

    /**
     * setFile1
     *
     * @param file1 setFile1
     */
    public void setFile1(String file1) {
        mFile1 = file1;
    }

    /**
     * getFile2
     *
     * @return getFile2
     */
    public String getFile2() {
        return mFile2;
    }

    /**
     * setFile2
     *
     * @param file2 setFile2
     */
    public void setFile2(String file2) {
        mFile2 = file2;
    }

    /**
     * getFile3
     *
     * @return getFile3
     */
    public String getFile3() {
        return mFile3;
    }

    /**
     * setFile3
     *
     * @param file3 setFile3
     */
    public void setFile3(String file3) {
        mFile3 = file3;
    }
}