package com.yanzhenjie.nohttp_harmonyos;

import com.yanzhenjie.nohttp_harmonyos.base.BaseAbility;
import com.yanzhenjie.nohttp_harmonyos.slice.MainAbilitySlice;
import com.yanzhenjie.nohttp_harmonyos.utils.Delivery;
import com.yanzhenjie.nohttp_harmonyos.utils.PermissionsUtils;
import com.yanzhenjie.nohttp_harmonyos.utils.Toast;
import com.yanzhenjie.nohttp.InitializationConfig;
import com.yanzhenjie.nohttp.Logger;
import com.yanzhenjie.nohttp.NoHttp;
import com.yanzhenjie.nohttp.cache.DBCacheStore;
import com.yanzhenjie.nohttp.cookie.DBCookieStore;
import com.yanzhenjie.nohttp.OkHttpNetworkExecutor;

import ohos.bundle.IBundleManager;

import static com.yanzhenjie.nohttp_harmonyos.utils.PermissionsUtils.REQUEST_READ_CODE;
import static com.yanzhenjie.nohttp_harmonyos.utils.PermissionsUtils.REQUEST_WRITE_CODE;

/**
 * String
 */
public class MainAbility extends BaseAbility {
    @Override
    protected String getEntry() {
        return MainAbilitySlice.class.getName();
    }

    @Override
    protected void initView() {

        // 网络初始化
        initHttp();

        // 权限
        Delivery.getInstance().postDelayed(new Runnable() {
            @Override
            public void run() {
                PermissionsUtils.getReadPermission(getContext());
            }
        }, 1000);
    }

    @Override
    protected void initData() {
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_CODE:
                permissionResults(false, grantResults);
                break;
            case REQUEST_READ_CODE:
                permissionResults(true, grantResults);
                return;
        }
    }

    private void permissionResults(boolean bl, int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
            // 权限被授予
            Toast.show(getContext(), bl ? "读取权限请求成功！！！" : "写入权限请求成功！！！");
            if (bl) {
                PermissionsUtils.getWritePermission(getContext());
            }

        } else {
            // 权限被拒绝
            Toast.show(getContext(), bl ? "读取权限请求失败！！！" : "写入权限请求失败！！！");
            if (bl) {
                PermissionsUtils.getWritePermission(getContext());
            }
        }
    }

    private void initHttp() {
        Toast.show(this, "来了老弟");
        // 开启Log日志
        Logger.setDebug(true); // 开启NoHttp的调试模式, 配置后可看到请求过程、日志和错误信息。
        Logger.setTag("NoHttp_"); // 打印Log的tag。

        InitializationConfig config = InitializationConfig.newBuilder(this)
                // 全局连接服务器超时时间，单位毫秒，默认10s。
                .connectionTimeout(30 * 1000)
                // 全局等待服务器响应超时时间，单位毫秒，默认10s。
                .readTimeout(30 * 1000)
                // 配置缓存，默认保存数据库DBCacheStore，保存到SD卡使用DiskCacheStore。
                .cacheStore(
                        // 如果不使用缓存，setEnable(false)禁用。
                        new DBCacheStore(getContext()).setEnable(true)
                )
                // 配置Cookie，默认保存数据库DBCookieStore，开发者可以自己实现CookieStore接口。
                .cookieStore(
                        // 如果不维护cookie，setEnable(false)禁用。
                        new DBCookieStore(getContext()).setEnable(false)
                )
                // 配置网络层，默认URLConnectionNetworkExecutor，如果想用OkHttp：OkHttpNetworkExecutor。
                .networkExecutor(new OkHttpNetworkExecutor())
                .retry(1) // 全局重试次数，配置后每个请求失败都会重试x次。
                .build();
        // 初始化
        NoHttp.initialize(config);
    }
}
