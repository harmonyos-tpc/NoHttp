/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.slice;

import com.yanzhenjie.nohttp.utils.LogUtil;
import com.yanzhenjie.nohttp_harmonyos.ResourceTable;
import com.yanzhenjie.nohttp_harmonyos.base.BaseAbilitySlice;
import com.yanzhenjie.nohttp_harmonyos.config.UrlConfig;
import com.yanzhenjie.nohttp_harmonyos.entity.FileInfo;
import com.yanzhenjie.nohttp_harmonyos.http.EntityRequest;
import com.yanzhenjie.nohttp_harmonyos.http.HttpCallback;
import com.yanzhenjie.nohttp_harmonyos.http.Result;
import com.yanzhenjie.nohttp_harmonyos.utils.FileUtils;
import com.yanzhenjie.nohttp_harmonyos.utils.Toast;
import com.yanzhenjie.nohttp.FileBinary;
import com.yanzhenjie.nohttp.Logger;
import com.yanzhenjie.nohttp.RequestMethod;
import com.yanzhenjie.nohttp.SimpleUploadListener;
import com.yanzhenjie.nohttp.utils.TextUtils;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.io.File;
import java.io.IOException;

/**
 * FormPresenterSlice
 */
public class FormPresenterSlice extends BaseAbilitySlice implements Component.ClickedListener {

    private Text selectFile;
    private Text beginForm;
    private Text formSelect;
    private String filePath = "";

    @Override
    protected int getLayout() {
        return ResourceTable.Layout_layout_form_presenter;
    }

    @Override
    protected void initView() {
        selectFile = (Text) findComponentById(ResourceTable.Id_select_file);
        beginForm = (Text) findComponentById(ResourceTable.Id_begin_form);
        formSelect = (Text) findComponentById(ResourceTable.Id_tv_form_select);
        beginForm.setClickedListener(this);
        selectFile.setClickedListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_select_file: // 选择文件
                try {
                    // 创建本地txt文件
                    filePath = FileUtils.createFile(getContext(), "来了老弟！！！");
                    formSelect.setText(filePath);
                } catch (IOException e) {
                    LogUtil.info("ok",e.getMessage());
                }
                break;
            case ResourceTable.Id_begin_form: // 开始上传
                if (TextUtils.isEmpty(filePath)) {
                    Toast.show(getContext(), "文件路径为空！！！");
                } else {
                    executeUpload();
                }
                break;
        }
    }

    /**
     * 表单上传
     */
    public void executeUpload() {
        FileBinary binary1 = new FileBinary(new File(filePath));
        binary1.setUploadListener(0, new SimpleUploadListener() {
            @Override
            public void onProgress(int what, int progress) {
                Logger.i(" =" + progress);
            }
        });
        EntityRequest<FileInfo> request =
                new EntityRequest<>(UrlConfig.UPLOAD_FORM, RequestMethod.POST, FileInfo.class);
        request.add("name", "nohttp")
                .add("age", 18)
                .add("file1", binary1);
        request(request, new HttpCallback<FileInfo>() {
            @Override
            public void onResponse(Result<FileInfo> response) {
                if (response.isSucceed()) {
                    formSelect.setText("上传成功");

                }
            }
        });
    }
}
