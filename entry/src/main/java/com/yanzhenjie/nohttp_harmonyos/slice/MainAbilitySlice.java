/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.slice;

import com.yanzhenjie.nohttp_harmonyos.ResourceTable;
import com.yanzhenjie.nohttp_harmonyos.base.BaseAbilitySlice;
import com.yanzhenjie.nohttp.download.DownloadRequest;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * MainAbilitySlice
 */
public class MainAbilitySlice extends BaseAbilitySlice implements Component.ClickedListener {
    private Button getBut;
    private Button postBut;
    private Button buttonPackage;
    private Button buttonDownload;
    String path = "";
    private DownloadRequest mRequest;

    @Override
    protected int getLayout() {
        return ResourceTable.Layout_layout_main;
    }

    @Override
    protected void initView() {
        Component componentById3= findComponentById(ResourceTable.Id_button_get);
        if (componentById3 instanceof Button) {
            getBut=(Button)componentById3;
        }


        Component componentById2= findComponentById(ResourceTable.Id_button_uploading);
        if (componentById2 instanceof Button) {
            postBut=(Button)componentById2;
        }


        Component componentById1 = findComponentById(ResourceTable.Id_button_package);
        if (componentById1 instanceof Button) {
            buttonPackage=(Button)componentById1;
        }

        Component componentById = findComponentById(ResourceTable.Id_button_download);
        if (componentById instanceof Button) {
            buttonDownload = (Button) componentById;
        }
        getBut.setClickedListener(this);
        postBut.setClickedListener(this);
        buttonPackage.setClickedListener(this);
        buttonDownload.setClickedListener(this);
    }

    @Override
    protected void initData() {
    }


    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_button_get: // 普通请求
//                startAbilitySlice(Routing.ACTION_COMMON);
                present(new CommonRequestSlice(),new Intent());
                break;
            case ResourceTable.Id_button_uploading: // 表单上传文件
                present(new FormPresenterSlice(),new Intent());
                break;
            case ResourceTable.Id_button_package: // 自定义上传包体
                present(new BodyPresenterSlice(),new Intent());
                break;
            case ResourceTable.Id_button_download: // 下载文件
                present(new DownloadAbilitySlice(),new Intent());
                break;
        }
    }

}
