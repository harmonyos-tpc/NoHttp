package com.yanzhenjie.nohttp_harmonyos.slice;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.yanzhenjie.nohttp_harmonyos.ResourceTable;
import com.yanzhenjie.nohttp_harmonyos.base.BaseAbilitySlice;
import com.yanzhenjie.nohttp_harmonyos.http.HttpCallback;
import com.yanzhenjie.nohttp_harmonyos.http.Result;
import com.yanzhenjie.nohttp_harmonyos.http.StringRequest;
import com.yanzhenjie.nohttp.RequestMethod;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * CommonRequestSlice
 */
public class CommonRequestSlice extends BaseAbilitySlice implements Component.ClickedListener {
    private Text commonGet;
    private Text commonPost;
    private Text commonResult;

    @Override
    protected int getLayout() {
        return ResourceTable.Layout_layout_common_request;
    }

    @Override
    protected void initView() {
        commonGet = (Text) findComponentById(ResourceTable.Id_common_get);
        commonPost = (Text) findComponentById(ResourceTable.Id_common_post);
        commonResult = (Text) findComponentById(ResourceTable.Id_common_result);
        commonGet.setClickedListener(this);
        commonPost.setClickedListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(Component component) {
        commonResult.setText("");
        switch (component.getId()) {
            case ResourceTable.Id_common_get:
                getHttp();
                break;
            case ResourceTable.Id_common_post:
                postHttp();
                break;

        }

    }

    private void getHttp() {
        String url = "https://httpbin.org/get";
        StringRequest request = new StringRequest(url, RequestMethod.GET);
        request(request, new HttpCallback<String>() {
            @Override
            public void onResponse(Result<String> response) {
                commonResult.setText(response.get());
            }
        });
    }

    private void postHttp() {
        String url = "https://httpbin.org/post";
        StringRequest request = new StringRequest(url, RequestMethod.POST);
        request(request, new HttpCallback<String>() {
            @Override
            public void onResponse(Result<String> response) {
                commonResult.setText(response.get());
            }
        });
    }
}
