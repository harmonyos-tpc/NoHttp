package com.yanzhenjie.nohttp_harmonyos.slice;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.yanzhenjie.nohttp.utils.LogUtil;
import com.yanzhenjie.nohttp_harmonyos.ResourceTable;
import com.yanzhenjie.nohttp_harmonyos.base.BaseAbilitySlice;
import com.yanzhenjie.nohttp_harmonyos.config.UrlConfig;
import com.yanzhenjie.nohttp_harmonyos.entity.FileInfo;
import com.yanzhenjie.nohttp_harmonyos.http.EntityRequest;
import com.yanzhenjie.nohttp_harmonyos.http.HttpCallback;
import com.yanzhenjie.nohttp_harmonyos.http.Result;
import com.yanzhenjie.nohttp_harmonyos.utils.FileUtils;
import com.yanzhenjie.nohttp_harmonyos.utils.Toast;
import com.yanzhenjie.nohttp.Logger;
import com.yanzhenjie.nohttp.RequestMethod;
import com.yanzhenjie.nohttp.utils.TextUtils;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.io.*;

/**
 * BodyPresenterSlice
 */
public class BodyPresenterSlice extends BaseAbilitySlice implements Component.ClickedListener {

    private Text tvBody;
    private Text tvPload;
    private Text selectingFile;
    private String filePath = "";

    @Override
    protected int getLayout() {
        return ResourceTable.Layout_layout_body_presenter;
    }

    @Override
    protected void initView() {
        tvBody = (Text) findComponentById(ResourceTable.Id_tvBody);
        tvPload = (Text) findComponentById(ResourceTable.Id_tv_pload);
        selectingFile = (Text) findComponentById(ResourceTable.Id_selecting_file);
        tvPload.setClickedListener(this);
        selectingFile.setClickedListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_tv_pload: // 开始上传
                // data/user/0/com.example.nohttp_harmonyos/files/NoHttp/filename.txt
                Toast.show(getContext(), "开始上传");
                if(TextUtils.isEmpty(filePath)){
                    Toast.show(getContext(),"文件路径为空！！！");
                }else {
                    try {
                        executeUpload();
                    } catch (IOException e) {
                        LogUtil.info("ok~~~", e.getMessage());
                    }
                }
                break;
            case ResourceTable.Id_selecting_file: // 选择文件
                Toast.show(getContext(), "选择文件");
                try {
                    // 创建本地txt文件
                    filePath = FileUtils.createFile(getContext(), "来了老弟！！！");
                    tvBody.setText(filePath);
                } catch (IOException e) {
                    LogUtil.info("ok~~~", e.getMessage());
                }
                break;
        }
    }


    /**
     * 包体上传
     *
     * @throws com.yanzhenjie.nohttp.json.JSONException executeUpload
     */
    public void executeUpload() throws IOException {
        File file = new File(filePath);
        InputStream fileStream = null;
        try {
            fileStream = new FileInputStream(file);
        } catch (FileNotFoundException ignored) {
            throw new AssertionError("omg.");
        }finally {
            fileStream.close();
        }

        EntityRequest<FileInfo> request =
                new EntityRequest<>(UrlConfig.UPLOAD_BODY_FILE, RequestMethod.POST, FileInfo.class);
        // MIME类型 无相关映射 contentType暂时写死  、image/jpeg  、image/png 、multipart/form-data、text/plain
        request.setDefineRequestBody(fileStream, "text/plain");
        request(request, new HttpCallback<FileInfo>() {
            @Override
            public void onResponse(Result<FileInfo> response) {
                if (response.isSucceed()) {
                    Logger.i("上传成功");
                    tvBody.setText("上传成功");
                }
            }
        });
    }
}