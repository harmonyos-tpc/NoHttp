/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.slice;

import com.yanzhenjie.nohttp_harmonyos.ResourceTable;
import com.yanzhenjie.nohttp_harmonyos.base.BaseAbilitySlice;
import com.yanzhenjie.nohttp_harmonyos.config.AppConfig;
import com.yanzhenjie.nohttp_harmonyos.config.UrlConfig;
import com.yanzhenjie.nohttp_harmonyos.http.Download;
import com.yanzhenjie.nohttp.Headers;
import com.yanzhenjie.nohttp.Logger;
import com.yanzhenjie.nohttp.RequestMethod;
import com.yanzhenjie.nohttp.download.DownloadListener;
import com.yanzhenjie.nohttp.download.DownloadRequest;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * DownloadAbilitySlice
 */
public class DownloadAbilitySlice extends BaseAbilitySlice implements Component.ClickedListener {
    private Text butDownload;
    private Text tvPath;
    private DownloadRequest mRequest;

    @Override
    protected int getLayout() {
        return ResourceTable.Layout_layout_download;
    }

    @Override
    protected void initView() {
        butDownload = (Text) findComponentById(ResourceTable.Id_but_download);
        tvPath = (Text) findComponentById(ResourceTable.Id_tv_path);
        butDownload.setClickedListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_but_download:
                tvPath.setText("正在下载...");
                buttonDownload();
                break;
        }
    }

    /**
     * 下载
     */
    public void buttonDownload() {
        String dir = AppConfig.get(this).PATH_APP_DOWNLOAD;
        mRequest = new DownloadRequest(UrlConfig.DOWNLOAD, RequestMethod.GET, dir, "yuxuhao.zip", true, true);
        mRequest.setCancelSign(this);
        Download.getInstance().download(0, mRequest, new DownloadListener() {
            @Override
            public void onDownloadError(int what, Exception exception) {
                mRequest = null;
            }

            @Override
            public void onStart(int what, boolean isResume, long rangeSize, Headers responseHeaders, long allCount) {
                Logger.i("rangeSize=" + rangeSize + "      allCount=" + allCount);
            }

            @Override
            public void onProgress(int what, int progress, long fileCount, long speed) {
                // 源码得知这里的progress 不是必徐有值的 具体查看-Downloader类
                Logger.i("progress=" + progress + "      fileCount=" + fileCount);

            }

            @Override
            public void onFinish(int what, String filePath) {
                mRequest = null;
                tvPath.setText("下载成功  ="+filePath);
                Logger.i("filePath=   " + filePath);
            }

            @Override
            public void onCancel(int what) {
                mRequest = null;
            }
        });
    }
}
