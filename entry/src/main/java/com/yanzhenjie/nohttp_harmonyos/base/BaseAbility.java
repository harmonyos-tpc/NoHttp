/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.base;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;

/**
 * Ability
 */
public abstract class BaseAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(getEntry());
        initView();
        initData();
    }


    /**
     * 无传参
     *
     * @param cl String
     */
    public void startAbility(Class cl) {
        Intent ii = new Intent();
        Operation op = new Intent.OperationBuilder()
                .withBundleName(this.getBundleName())
                .withAbilityName(cl.getName())
                .build();
        ii.setOperation(op);
        startAbility(ii);
    }


    /**
     * 有传参
     *
     * @param cl startAbility
     * @param parameters startAbility
     */
    public void startAbility(Class cl, IntentParams parameters) {
        Intent ii = new Intent();
        Operation op = new Intent.OperationBuilder()
                .withBundleName(this.getBundleName())
                .withAbilityName(cl.getName())
                .build();
        ii.setOperation(op);
        ii.setParams(parameters);
        startAbility(ii);
    }

    /**
     * 获取RouteName
     *
     * @return String
     */
    protected abstract String getEntry();


    /**
     * 初始化Veiw
     */
    protected abstract void initView();

    /**
     * data处理
     */

    protected abstract void initData();
}
