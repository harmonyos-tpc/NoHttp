/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.base;

import com.yanzhenjie.nohttp_harmonyos.http.AbstractRequest;
import com.yanzhenjie.nohttp_harmonyos.http.CallServer;
import com.yanzhenjie.nohttp_harmonyos.http.Download;
import com.yanzhenjie.nohttp_harmonyos.http.HttpCallback;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;

/**
 * AbilitySlice
 */
public abstract class BaseAbilitySlice extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(getLayout());

        initView();
        initData();
    }

    /**
     * sdf
     *
     * @return getLayout
     */
    protected abstract int getLayout();

    /**
     * 初始化Veiw
     */
    protected abstract void initView();

    /**
     * Data处理
     */
    protected abstract void initData();

    /**
     * 无传参
     *
     * @param action String
     */
    public void startAbilitySlice(String action) {
        Intent secondIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withAction(action)
                .build();
        secondIntent.setOperation(operation);
        startAbility(secondIntent);
    }

    public void starAbilityXXX(String withAbilityName){
        Intent intent = new Intent();

        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.yanzhenjie.nohttp_harmonyos.slice")
                .withAbilityName(withAbilityName)
                .build();

        intent.setOperation(operation);
        startAbility(intent);
    }

    /**
     * 有传参
     *
     * @param parameters String
     * @param action String
     */
    public void startAbilitySlice(String action, IntentParams parameters) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withAction(action)
                .build();
        intent.setOperation(operation);
        intent.setParams(parameters);
        startAbility(intent);
    }


    /**
     * 异步请求。
     *
     * @param request String
     * @param httpCallback String
     *
     */
    public <T> void request(AbstractRequest<T> request, HttpCallback<T> httpCallback) {
        request.setCancelSign(this);
        CallServer.getInstance().request(this, request, httpCallback);
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        CallServer.getInstance().cancelBySign(this);
        Download.getInstance().cancelBySign(this);
        super.onStop();
    }
}
