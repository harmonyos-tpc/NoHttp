/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.utils;

import ohos.app.Context;
import ohos.bundle.IBundleManager;

/**
 *  PermissionsUtils
 */
public class PermissionsUtils {
    /**
     * REQUEST_READ_CODE
     */
    public static final int REQUEST_WRITE_CODE = 1000001;
    /**
     * REQUEST_READ_CODE
     */
    public static final int REQUEST_READ_CODE = 1000002;

    /**
     * String
     *
     * @param context String
     */
    public static void getReadPermission(Context context) {
        /**
         * 读取权限申请
         * verifySelfPermission 如果已被授予权限，可以结束权限申请流程。如果未被授予权限，继续执行下一步。
         * canRequestPermission 查询是否可动态申请
         * requestPermissionsFromUser 动态申请权限，通过回调函数接受授予结果 ,是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
         */
        if (context.verifySelfPermission("ohos.permission.READ_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
            if (context.canRequestPermission("ohos.permission.READ_MEDIA")) {
                context.requestPermissionsFromUser(
                        new String[]{"ohos.permission.READ_MEDIA"}, REQUEST_READ_CODE);
            } else {
                Toast.show(context, "该权限已被禁用！！！");
            }
        } else {
            Toast.show(context, "该权限已被授予！！！");
        }
    }

    /**
     * String
     *
     * @param context String
     */
    public static void getWritePermission(Context context) {
        /**
         * 写入权限申请
         * verifySelfPermission 如果已被授予权限，可以结束权限申请流程。如果未被授予权限，继续执行下一步。
         * canRequestPermission 查询是否可动态申请
         * requestPermissionsFromUser 动态申请权限，通过回调函数接受授予结果 ,是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
         */
        if (context.verifySelfPermission("ohos.permission.WRITE_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
            if (context.canRequestPermission("ohos.permission.WRITE_MEDIA")) {
                context.requestPermissionsFromUser(
                        new String[]{"ohos.permission.WRITE_MEDIA"}, REQUEST_WRITE_CODE);
            } else {
                Toast.show(context, "该权限已被禁用！！！");
            }
        } else {
            Toast.show(context, "该权限已被授予！！！");
        }
    }
}
