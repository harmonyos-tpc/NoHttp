/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.utils;


import com.yanzhenjie.nohttp.Logger;
import com.yanzhenjie.nohttp.utils.LogUtil;
import com.yanzhenjie.nohttp.utils.TextUtils;

import ohos.app.Context;

import ohos.data.usage.DataUsage;
import ohos.data.usage.MountState;
import ohos.miscservices.pasteboard.PasteData;
import ohos.utils.net.Uri;

import java.io.*;

/**
 * Created by Yan Zhenjie on 2016/7/7.
 */
public class FileUtils {
    private Context context;

    /**
     *  getAppRootPath
     *
     * @param context getAppRootPath
     * @return getAppRootPath
     */
    public static File getAppRootPath(Context context) {
        if (sdCardIsAvailable(context)) {
            return context.getExternalCacheDir();
        } else {
            return context.getFilesDir();
        }
    }

    /**
     * sdCardIsAvailable
     *
     * @param context sdCardIsAvailable
     * @return sdCardIsAvailable
     */
    public static boolean sdCardIsAvailable(Context context) {
        if (DataUsage.getDiskMountedStatus().equals(MountState.DISK_MOUNTED)) {
            File sd = new File(context.getExternalCacheDir().getAbsolutePath());
            return sd.canWrite();
        } else {
            return false;
        }

    }

    /**
     * getMimeType
     *
     * @param url getMimeType
     * @return getMimeType
     */
    public static String getMimeType(String url) {
        // m这个方法不对 拿到的mimetype是不对的
        PasteData.Record uriRecord = PasteData.Record.createUriRecord(Uri.parse(url));

        String mimeType = uriRecord.getMimeType();

        return TextUtils.isEmpty(mimeType) ? "" : mimeType;
    }

    /**
     * 创建本地txt文件
     *
     * @param context String
     * @param st      String
     * @return String
     * @throws com.yanzhenjie.nohttp.json.JSONException String
     */
    public static String createFile(Context context, String st) throws IOException {
        String filePath = getAppRootPath(context).getAbsolutePath() + File.separator + "NoHttp";
        File dir = new File(filePath);
        boolean exists1 = dir.exists();
        if (!exists1) {
            if(dir.mkdirs()){
                LogUtil.info("ok","ok");
            }
        }
        File checkFile = new File(filePath + File.separator + "filename.txt");
        FileWriter writer = null;
        try {
            if (!checkFile.exists()) {
                if(checkFile.createNewFile()){
                    LogUtil.info("ok","ok");
                }
            }
            writer = new FileWriter(checkFile, true);
            writer.append(st);
            writer.flush();
            Logger.i("textFile = " + "创建成功");
        } catch (IOException e) {
            LogUtil.error("IOException", e.getMessage());
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
        return filePath + File.separator + "filename.txt";
    }

    /**
     * 读取指定路径txt内容
     *
     * @param path file
     * @throws com.yanzhenjie.nohttp.json.JSONException dds
     */
    public static void readTextFile(String path) throws IOException {
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            File file = new File(path);
            if (file.isFile() && file.exists()) {
                isr = new InputStreamReader(new FileInputStream(file), "utf-8");
                br = new BufferedReader(isr);
                String lineTxt = null;
                while ((lineTxt = br.readLine()) != null) {
                    Logger.i(lineTxt);
                }
                br.close();
            } else {
                Logger.i("文件不存在");
            }
        } catch (IOException e) {
            Logger.i("文件读取错误");
        } finally {
            isr.close();
            br.close();
        }
    }
}
