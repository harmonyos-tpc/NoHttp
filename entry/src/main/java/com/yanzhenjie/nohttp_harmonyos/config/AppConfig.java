/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yanzhenjie.nohttp_harmonyos.config;

import com.alibaba.fastjson.JSON;
import com.yanzhenjie.nohttp_harmonyos.utils.FileUtils;
import com.yanzhenjie.nohttp.tools.IOUtils;
import com.yanzhenjie.nohttp.utils.TextUtils;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import java.io.File;
import java.util.Optional;

/**
 * Created by YanZhenjie on 2018/2/27.
 */
public class AppConfig {
    private static AppConfig sInstance;

    /**
     * AppConfig AppConfig
     *
     * @param context AppConfig
     * @return AppConfig
     */
    public static AppConfig get(Context context) {
        if (sInstance == null) {
            synchronized (AppConfig.class) {
                if (sInstance == null) {
                    sInstance = new AppConfig(context);
                }
            }
        }
        return sInstance;
    }

    private final Preferences mPreferences;

    /**
     * App root path.
     */
    private final String PATH_APP_ROOT;

    /**
     * Download.
     */
    public final String PATH_APP_DOWNLOAD;

    /**
     * Images.
     */
    private final String PATH_APP_IMAGE;

    /**
     * Cache root path.
     */
    private final String PATH_APP_CACHE;

    private AppConfig(Context context) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        String fileName = "NoHttp";
        this.mPreferences = databaseHelper.getPreferences(fileName);

        this.PATH_APP_ROOT = FileUtils.getAppRootPath(context).getAbsolutePath() + File.separator + "NoHttp";
        this.PATH_APP_DOWNLOAD = PATH_APP_ROOT + File.separator + "Download";
        this.PATH_APP_IMAGE = PATH_APP_ROOT + File.separator + "Images";
        this.PATH_APP_CACHE = PATH_APP_ROOT + File.separator + "Cache";
    }

    /**
     * Initialize file system for app.
     */
    public void initFileDir() {
        IOUtils.createFolder(PATH_APP_ROOT);
        IOUtils.createFolder(PATH_APP_DOWNLOAD);
        IOUtils.createFolder(PATH_APP_IMAGE);
        IOUtils.createFolder(PATH_APP_CACHE);
    }

    /**
     * setStringApply
     *
     * @param key   key
     * @param value value
     */
    public void setStringApply(String key, String value) {
        mPreferences.putString(key, value);
        mPreferences.flush();
    }

    /**
     * value
     *
     * @param key   value
     * @param value value
     * @return value
     */
    public boolean setStringCommit(String key, String value) {
        mPreferences.putString(key, value);
        mPreferences.flush();
        return true;
    }

    /**
     * setBooleanApply
     *
     * @param key   setBooleanApply
     * @param value setBooleanApply
     */
    public void setBooleanApply(String key, boolean value) {
        mPreferences.putBoolean(key, value);
        mPreferences.flush();
    }

    /**
     * setBooleanApply
     *
     * @param key   setBooleanCommit
     * @param value setBooleanCommit
     * @return setBooleanCommit
     */
    public boolean setBooleanCommit(String key, boolean value) {
        mPreferences.putBoolean(key, value);
        mPreferences.flush();
        return true;
    }

    /**
     * setFloatApply
     *
     * @param key   setFloatApply
     * @param value setFloatApply
     */
    public void setFloatApply(String key, float value) {
        mPreferences.putFloat(key, value);
        mPreferences.flush();
    }

    /**
     * setFloatCommit
     *
     * @param key   setFloatCommit
     * @param value setFloatCommit
     * @return setFloatCommit
     */
    public boolean setFloatCommit(String key, float value) {
        mPreferences.putFloat(key, value);
        return true;
    }

    /**
     * setIntApply
     *
     * @param key   setIntApply
     * @param value setIntApply
     */
    public void setIntApply(String key, int value) {
        mPreferences.putInt(key, value);
        mPreferences.flush();
    }

    /**
     * setIntCommit
     *
     * @param key   setIntCommit
     * @param value setIntCommit
     * @return setIntCommit
     */
    public boolean setIntCommit(String key, int value) {
        mPreferences.putInt(key, value);
        return true;
    }

    /**
     * setIntCommit
     *
     * @param key   setIntCommit
     * @param value setIntCommit
     */
    public void setLongApply(String key, long value) {
        mPreferences.putLong(key, value);
        mPreferences.flush();
    }

    /**
     * setLongCommit
     *
     * @param key   setLongCommit
     * @param value setLongCommit
     * @return setLongCommit
     */
    public boolean setLongCommit(String key, long value) {
        mPreferences.putLong(key, value);
        mPreferences.flush();
        return true;
    }

    /**
     * setObjectApply
     *
     * @param key   setObjectApply
     * @param param setObjectApply
     * @param <P>   setObjectApply
     */
    public <P> void setObjectApply(String key, P param) {
        if (param != null) {
            mPreferences.putString(key, JSON.toJSONString(param));
            mPreferences.flush();
        } else {
            mPreferences.putString(key, "");
            mPreferences.flush();
        }
    }

    /**
     * setObjectCommit
     *
     * @param key   setObjectCommit
     * @param param setObjectCommit
     * @param <P>   setObjectCommit
     * @return setObjectCommit
     */
    public <P> boolean setObjectCommit(String key, P param) {
        if (param != null) {
            mPreferences.putString(key, JSON.toJSONString(param));
            mPreferences.flush();
            return true;
        } else {
            mPreferences.putString(key, "");
            mPreferences.flush();
            return true;
        }
    }

    /**
     * getString
     *
     * @param key          getString
     * @param defaultValue getString
     * @return getString
     */
    public String getString(String key, String defaultValue) {
        return mPreferences.getString(key, defaultValue);
    }

    /**
     * getBoolean
     *
     * @param key          getBoolean
     * @param defaultValue getBoolean
     * @return getBoolean
     */
    public Boolean getBoolean(String key, boolean defaultValue) {
        return mPreferences.getBoolean(key, defaultValue);
    }

    /**
     * getFloat
     *
     * @param key          getFloat
     * @param defaultValue getFloat
     * @return getFloat
     */
    public float getFloat(String key, float defaultValue) {
        return mPreferences.getFloat(key, defaultValue);
    }

    /**
     * getInt
     *
     * @param key          getInt
     * @param defaultValue getInt
     * @return getInt
     */
    public int getInt(String key, int defaultValue) {
        return mPreferences.getInt(key, defaultValue);
    }

    /**
     * getLong
     *
     * @param key          getLong
     * @param defaultValue getLong
     * @return getLong
     */
    public long getLong(String key, long defaultValue) {
        return mPreferences.getLong(key, defaultValue);
    }

    /**
     * getLong
     *
     * @param key    getLong
     * @param pClass getLong
     * @param <P>    getLong
     * @return getLong
     */
    public <P> P getObject(String key, Class<P> pClass) {
        String jsonObject = getString(key, "");
        if (!TextUtils.isEmpty(jsonObject)) {
            return JSON.parseObject(jsonObject, pClass);
        }
        return (P) Optional.empty();
    }
}
