package com.yanzhenjie.nohttp_harmonyos;

import com.yanzhenjie.nohttp.FileBinary;
import com.yanzhenjie.nohttp.Headers;
import com.yanzhenjie.nohttp.InitializationConfig;
import com.yanzhenjie.nohttp.Logger;
import com.yanzhenjie.nohttp.NoHttp;
import com.yanzhenjie.nohttp.OkHttpNetworkExecutor;
import com.yanzhenjie.nohttp.RequestMethod;
import com.yanzhenjie.nohttp.SimpleUploadListener;
import com.yanzhenjie.nohttp.cache.DBCacheStore;
import com.yanzhenjie.nohttp.cookie.DBCookieStore;
import com.yanzhenjie.nohttp.download.DownloadListener;
import com.yanzhenjie.nohttp.download.DownloadRequest;
import com.yanzhenjie.nohttp.utils.LogUtil;
import com.yanzhenjie.nohttp_harmonyos.config.AppConfig;
import com.yanzhenjie.nohttp_harmonyos.config.UrlConfig;
import com.yanzhenjie.nohttp_harmonyos.entity.FileInfo;
import com.yanzhenjie.nohttp_harmonyos.http.AbstractRequest;
import com.yanzhenjie.nohttp_harmonyos.http.CallServer;
import com.yanzhenjie.nohttp_harmonyos.http.Download;
import com.yanzhenjie.nohttp_harmonyos.http.EntityRequest;
import com.yanzhenjie.nohttp_harmonyos.http.HttpCallback;
import com.yanzhenjie.nohttp_harmonyos.http.Result;
import com.yanzhenjie.nohttp_harmonyos.http.StringRequest;
import com.yanzhenjie.nohttp_harmonyos.utils.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MainAbilityTest {
    private static final String LOGTAG = "NoHttp_test-->";
    private Boolean testBl = false;
    private String filePath = "";
    private DownloadRequest mRequest;

    @Before
    public void httpInit() {
        initHttp();
    }

    /**
     * 普通请求
     * Get
     */
    @Test
    public void commonRequestGet() {
        testBl = false;
        String url = "https://httpbin.org/get";
        StringRequest request = new StringRequest(url, RequestMethod.GET);
        request(request, new HttpCallback<String>() {
            @Override
            public void onResponse(Result<String> response) {
                testBl = true;
                LogUtil.info(LOGTAG, response.get());
            }
        });
        delayOperation(10);
    }

    /**
     * 普通请求
     * Post
     */
    @Test
    public void commonRequestPost() {
        testBl = false;
        String url = "https://httpbin.org/post";
        StringRequest request = new StringRequest(url, RequestMethod.POST);
        request(request, new HttpCallback<String>() {
            @Override
            public void onResponse(Result<String> response) {
                testBl = true;
                LogUtil.info(LOGTAG, response.get());
            }
        });
        delayOperation(10);
    }

    /**
     * 表单上传
     */
    @Test
    public void uploadingForm() {
        testBl = false;
        // 创建本地txt文件
        try {
            filePath = FileUtils.createFile(NoHttpHarmonyOS.getApp(), "来了老弟！！！");
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileBinary binary1 = new FileBinary(new File(filePath));
        binary1.setUploadListener(0, new SimpleUploadListener() {
            @Override
            public void onProgress(int what, int progress) {
                Logger.i(" =" + progress);
            }
        });
        EntityRequest<FileInfo> request =
                new EntityRequest<>(UrlConfig.UPLOAD_FORM, RequestMethod.POST, FileInfo.class);
        request.add("name", "nohttp")
                .add("age", 18)
                .add("file1", binary1);
        request(request, new HttpCallback<FileInfo>() {
            @Override
            public void onResponse(Result<FileInfo> response) {
                if (response.isSucceed()) {
                    testBl = true;
                    LogUtil.info(LOGTAG, "上传成功！！！");

                }
            }
        });
        delayOperation(30);
    }

    /**
     * 图片上传
     */
    @Test
    public void uploadPackage() {
        testBl = false;
        // 文件路径选择
        filePath = "";

        File file = new File(filePath);
        InputStream fileStream = null;
        try {
            fileStream = new FileInputStream(file);
        } catch (FileNotFoundException ignored) {
            throw new AssertionError("omg.");
        } finally {
            try {
                fileStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        EntityRequest<FileInfo> request =
                new EntityRequest<>(UrlConfig.UPLOAD_BODY_FILE, RequestMethod.POST, FileInfo.class);
        // MIME类型 无相关映射 contentType暂时写死  、image/jpeg  、image/png 、multipart/form-data、text/plain
        request.setDefineRequestBody(fileStream, "text/plain");
        request(request, new HttpCallback<FileInfo>() {
            @Override
            public void onResponse(Result<FileInfo> response) {
                if (response.isSucceed()) {
                    testBl = true;
                    Logger.i("上传成功");
                }
            }
        });

        delayOperation(30);
    }

    @Test
    public void DownloadFile() {
        testBl = false;
        String dir = AppConfig.get(NoHttpHarmonyOS.getApp()).PATH_APP_DOWNLOAD;
        mRequest = new DownloadRequest(UrlConfig.DOWNLOAD, RequestMethod.GET, dir, "yuxuhao.zip", true, true);
        mRequest.setCancelSign(this);
        Download.getInstance().download(0, mRequest, new DownloadListener() {
            @Override
            public void onDownloadError(int what, Exception exception) {
                mRequest = null;
            }

            @Override
            public void onStart(int what, boolean isResume, long rangeSize, Headers responseHeaders, long allCount) {
                Logger.i("rangeSize=" + rangeSize + "      allCount=" + allCount);
            }

            @Override
            public void onProgress(int what, int progress, long fileCount, long speed) {
                // 源码得知这里的progress 不是必徐有值的 具体查看-Downloader类
                Logger.i("progress=" + progress + "      fileCount=" + fileCount);

            }

            @Override
            public void onFinish(int what, String filePath) {
                mRequest = null;
                testBl = true;
                Logger.i("下载成功----filePath=   " + filePath);
            }

            @Override
            public void onCancel(int what) {
                mRequest = null;
            }
        });
        delayOperation(30);
    }
    //=============================================基础配置=======================================================

    public <T> void request(AbstractRequest<T> request, HttpCallback<T> httpCallback) {
        request.setCancelSign(this);
        CallServer.getInstance().request(NoHttpHarmonyOS.getApp(), request, httpCallback);
    }

    /**
     * 做个延迟 打印测试结果
     */
    private void delayOperation(long seconds) {
        try {
            Thread.sleep(1000 * seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(testBl);
    }

    /**
     * 初始化
     */
    private void initHttp() {
        LogUtil.info(LOGTAG, "------初始化-----");
        // 开启Log日志
        Logger.setDebug(true); // 开启NoHttp的调试模式, 配置后可看到请求过程、日志和错误信息。
        Logger.setTag("NoHttp_"); // 打印Log的tag。

        InitializationConfig config = InitializationConfig.newBuilder(NoHttpHarmonyOS.getApp())
                // 全局连接服务器超时时间，单位毫秒，默认10s。
                .connectionTimeout(30 * 1000)
                // 全局等待服务器响应超时时间，单位毫秒，默认10s。
                .readTimeout(30 * 1000)
                // 配置缓存，默认保存数据库DBCacheStore，保存到SD卡使用DiskCacheStore。
                .cacheStore(
                        // 如果不使用缓存，setEnable(false)禁用。
                        new DBCacheStore(NoHttpHarmonyOS.getApp()).setEnable(true)
                )
                // 配置Cookie，默认保存数据库DBCookieStore，开发者可以自己实现CookieStore接口。
                .cookieStore(
                        // 如果不维护cookie，setEnable(false)禁用。
                        new DBCookieStore(NoHttpHarmonyOS.getApp()).setEnable(false)
                )
                // 配置网络层，默认URLConnectionNetworkExecutor，如果想用OkHttp：OkHttpNetworkExecutor。
                .networkExecutor(new OkHttpNetworkExecutor())
                .retry(1) // 全局重试次数，配置后每个请求失败都会重试x次。
                .build();
        // 初始化
        NoHttp.initialize(config);
    }
}
